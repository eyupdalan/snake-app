import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Game from "./src/components/game";

export default function App() {
  return (
    <View style={styles.container}>
      <Game style={styles.game} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  game: {
    marginTop: 30
  }
});
