import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { DEFAULT_CELL_WIDTH } from "../../constants";

type CellProps = {
    cellWidth?: number,
    isFood?: boolean,
    isSnake?: boolean
}

export default class Cell extends Component<CellProps, {}> {
    public static defaultProps: CellProps = {
        cellWidth: 10
    }

    get className(): object {
        return this.props.isFood ? styles.food : (this.props.isSnake ? styles.snake : null);
    }

    render() {
        return (
            <View style={[styles.cell, this.className]} />
        );
    }
}

const styles = StyleSheet.create({
    cell: {
        width: DEFAULT_CELL_WIDTH,
        height: DEFAULT_CELL_WIDTH,
        borderColor: "lightgray",
        borderStyle: "solid",
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0
    },
    food: {
        backgroundColor: "red"
    },
    snake: {
        backgroundColor: "orange"
    }
});