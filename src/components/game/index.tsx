import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { View, Text } from 'react-native';
import Grid from "../grid";
import { DEFAULT_CELL_WIDTH } from "../../constants";
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import GestureUtils from "../../utils/gestureUtils";

const { SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT } = swipeDirections;

type GameProps = {
    style: any
}

type GameState = {
    columnsCount: number,
    rowsCount: number,
    snake?: any[],
    snakeDirection?: string,
    food?: any
}

export default class Game extends Component<GameProps, GameState> {
    moveSnakeInterval: any;

    constructor(props) {
        super(props);
        const screenWidth = Math.round(Dimensions.get('window').width);
        const screenHeight = Math.round(Dimensions.get('window').height);

        const cols = Math.floor(screenWidth / DEFAULT_CELL_WIDTH) - 2;
        const rows = Math.floor(screenHeight / DEFAULT_CELL_WIDTH) - 5;

        this.state = {
            columnsCount: cols,
            rowsCount: rows
        }
    }
    componentDidMount() {
        this.startGame();
    }

    componentWillUnmount() {
        this.endGame();
    }

    startGame() {
        this.removeTimers();
        this.moveSnakeInterval = setInterval(this.moveSnake, 500);
        this.createFood();
        this.setState({
            snake: [[5, 5]],
            snakeDirection: SWIPE_DOWN
        })
    }

    endGame = () => {
        this.removeTimers();
    }

    moveSnake = () => {
        const newSnake = [];
        switch (this.state.snakeDirection) {
            case SWIPE_UP:
                newSnake[0] = [this.state.snake[0][0], this.state.snake[0][1] - 1]
                break;
            case SWIPE_DOWN:
                newSnake[0] = [this.state.snake[0][0], this.state.snake[0][1] + 1]
                break;
            case SWIPE_LEFT:
                newSnake[0] = [this.state.snake[0][0] - 1, this.state.snake[0][1]]
                break;
            case SWIPE_RIGHT:
                newSnake[0] = [this.state.snake[0][0] + 1, this.state.snake[0][1]]
                break;
        }

        [].push.apply(newSnake, this.state.snake.slice(1).map((s, i) => { return this.state.snake[i] }));
        this.setState({ snake: newSnake });

        //ate food?
        this.checkIfAteFood(newSnake);

        //valid move?
        //end game
        if (!this.isValid(newSnake[0]) || !this.doesntOverlap(newSnake)) {
            // end the game
            this.endGame()
        }

    }

    createFood = () => {
        const x = Math.ceil(Math.random() * this.state.columnsCount);
        const y = Math.ceil(Math.random() * this.state.rowsCount);
        this.setState({ food: [x, y] });
    }

    shallowEquals(arr1, arr2) {
        if (!arr1 || !arr2 || arr1.length !== arr2.length) return false;
        let equals = true;
        for (var i = 0; i < arr1.length; i++) {
            if (arr1[i] !== arr2[i]) equals = false;
        }
        return equals;
    }

    arrayDiff(arr1, arr2) {
        return arr1.map((a, i) => {
            return a - arr2[i]
        })
    }

    doesntOverlap(snake) {
        return (
            snake.slice(1).filter(c => {
                return this.shallowEquals(snake[0], c);
            }).length === 0
        );
    }

    checkIfAteFood(newSnake) {
        if (!this.shallowEquals(newSnake[0], this.state.food)) {
            return
        }
        // snake gets longer
        let newSnakeSegment;
        const lastSegment = newSnake[newSnake.length - 1];

        // where should we position the new snake segment?
        // here are some potential positions, we can choose the best looking one
        let lastPositionOptions = [[-1, 0], [0, -1], [1, 0], [0, 1]];

        // the snake is moving along the y-axis, so try that instead
        if (newSnake.length > 1) {
            lastPositionOptions[0] = this.arrayDiff(lastSegment, newSnake[newSnake.length - 2]);
        }

        for (var i = 0; i < lastPositionOptions.length; i++) {
            newSnakeSegment = [
                lastSegment[0] + lastPositionOptions[i][0],
                lastSegment[1] + lastPositionOptions[i][1]
            ];
            if (this.isValid(newSnakeSegment)) {
                break;
            }
        }

        this.setState({
            snake: newSnake.concat([newSnakeSegment]),
            food: []
        });
        this.createFood();
    }

    isValid(cell) {
        return (
            cell[0] > -1 &&
            cell[1] > -1 &&
            cell[0] < this.state.columnsCount &&
            cell[1] < this.state.rowsCount
        );
    }

    removeTimers = () => {
        if (this.moveSnakeInterval) {
            clearInterval(this.moveSnakeInterval);
        }
    }

    onSwipe = (gestureName: string, gestureState: any): void => {
        console.log(gestureName);
        console.log(gestureState);
        const gestureNameTemp = GestureUtils.getSwipeDirection(gestureState);
        if (gestureNameTemp === null || gestureNameTemp === undefined) {
            return;
        }

        if (gestureNameTemp === this.state.snakeDirection) {
            return;
        }

        if ((gestureNameTemp === SWIPE_UP && this.state.snakeDirection === SWIPE_DOWN) || (gestureNameTemp === SWIPE_DOWN && this.state.snakeDirection === SWIPE_UP)) {
            return;
        }

        if ((gestureNameTemp === SWIPE_RIGHT && this.state.snakeDirection === SWIPE_LEFT) || (gestureNameTemp === SWIPE_LEFT && this.state.snakeDirection === SWIPE_RIGHT)) {
            return;
        }

        this.setState({ snakeDirection: gestureNameTemp });
    }

    render() {
        return (
            <GestureRecognizer
                onSwipe={this.onSwipe}
                config={GestureUtils.config}
            >
                <Grid
                    style={this.props.style}
                    rowsCount={this.state.rowsCount}
                    columnsCount={this.state.columnsCount}
                    snake={this.state.snake}
                    food={this.state.food}
                />
            </GestureRecognizer>
        );
    }
}