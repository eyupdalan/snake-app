import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Cell from '../cell';

type GridProps = {
    style: any,
    rowsCount: number,
    columnsCount: number,
    snake?: any[],
    food?: number[]
}

export default class Grid extends Component<GridProps, {}> {
    get cells(): object[] {
        const items = [];

        for (let i = 0; i < this.props.rowsCount; i++) {
            const rowItems = [];
            for (let j = 0; j < this.props.columnsCount; j++) {
                const isSnake = this.props.snake && (this.props.snake.some(item => item[0] === j && item[1] === i));
                const isFood = this.props.food && (this.props.food[0] === j && this.props.food[1] === i);

                rowItems.push(<Cell key={`cell-${i}${j}`} isSnake={isSnake} isFood={isFood} />);
            }
            items.push(<View key={`row-${i}`} style={styles.row}>{rowItems}</View>);

        }
        return items;
    }

    render() {
        return (
            <View style={[this.props.style, styles.grid]}>
                {this.cells}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    grid: {
        borderColor: "lightgray",
        borderStyle: "solid",
        borderWidth: 1,
        borderBottomWidth: 0,
        borderRightWidth: 0
    },
    row: {
        display: "flex",
        flexDirection: "row",
    }
});