import { swipeDirections } from 'react-native-swipe-gestures';

export default class GestureUtils {
    static config = {
        velocityThreshold: 0,
        directionalOffsetThreshold: 200
    };

    static isValidSwipe(
        velocity: number,
        velocityThreshold: number,
        directionalOffset: number,
        directionalOffsetThreshold: number
    ): boolean {
        return (
            Math.abs(velocity) > velocityThreshold &&
            Math.abs(directionalOffset) < directionalOffsetThreshold
        );
    }

    static getSwipeDirection(gestureState) {
        const { SWIPE_LEFT, SWIPE_RIGHT, SWIPE_UP, SWIPE_DOWN } = swipeDirections;
        const { dx, dy, vx, vy } = gestureState;
        if (Math.abs(vx) > Math.abs(vy) && this._isValidHorizontalSwipe(gestureState)) {
            return dx > 0 ? SWIPE_RIGHT : SWIPE_LEFT;
        } else if (Math.abs(vy) > Math.abs(vx) && this._isValidVerticalSwipe(gestureState)) {
            return dy > 0 ? SWIPE_DOWN : SWIPE_UP;
        }
        return null;
    }


    static _isValidHorizontalSwipe(gestureState) {
        const { vx, dy } = gestureState;
        const { velocityThreshold, directionalOffsetThreshold } = GestureUtils.config;
        return GestureUtils.isValidSwipe(vx, velocityThreshold, dy, directionalOffsetThreshold);
    }

    static _isValidVerticalSwipe(gestureState) {
        const { vy, dx } = gestureState;
        const { velocityThreshold, directionalOffsetThreshold } = GestureUtils.config;
        return GestureUtils.isValidSwipe(vy, velocityThreshold, dx, directionalOffsetThreshold);
    }
}   